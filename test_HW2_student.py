import unittest
import subprocess
import requests
import sys
import random
import time
import argparse


class TestKVS(unittest.TestCase):
    '''
    Creating a subnet:
        sudo docker network create --subnet=10.0.0.0/16 mynet
    '''
    ip_addresses = ['10.0.0.20', '10.0.0.21', '10.0.0.22']
    host_ports = ['8083', '8084', '8085']
    node_ids = []
    nodes_address = []
    has_been_run_once = False
    all_tests_done = False
    key1 = '_yJnRhMUqNHXlUvxTie9jfQ0n6DX2of2ET13aW1LGLnvF9ZBxowE5NluZ3bH0Ctlw65S6XjYftCIzIIWDRd8bS5ykWKZvZGVvQDvakcODw___yiN8purA8Xfl_9WOzCLGYyJF4K2q3yOaOnd6Iu9SEo'
    key2 = '6TLxbmwMTN4hX7L0QX5_NflWH0QKfrTlzcuM5PUQHS52___lCzKbEMxLZHhtfww3KcMoboDLjB6mw_wFfEz5v_TtHqvGOZnk4_8aqHga79BaHXzpU9_IRbdjYdQutAU0HEuji6Ny1Ol'
    key3 = '6TLxbmwMTN4hX7L0QX5_NflWH0QKfrTlzcuM5PUQHS52___lCizKbEMxLZHhtfww3KcMoboDLjB6mw_wFfEz5v_TtHqvGOZnk4_8aqHga79BaHXzpU9_IRbdjYdQutAU0HEuji6Ny1Ol_MSaBF4JdT0aiG_N7xAkoPH3AlmVqDN45KDGBz7_YHrLnbLEK11SQxZcKXbFomh9JpH_sbqXIaifqOy4g06Ab0q3WkNfVzx7H0hGhNlkINf5PF12'
    val1 = 'aaaasjdhvksadjfbakdjs'
    val2 = 'asjdhvksadjfbakdjs'
    val3 = 'bknsdfnbKSKJDBVKpnkjgbsnk'

    def spin_up_nodes(self):

        exec_string_main = sudo + " docker run -p 8083:8080 --net=mynet --ip=10.0.0.20 -d %s" % contname
        print(exec_string_main)
        self.__class__.node_ids.append(subprocess.check_output(exec_string_main, shell=True).strip('\n'))

        exec_string_forw1 = sudo + " docker run -p 8084:8080 --net=mynet -e MAINIP=10.0.0.20:8080 -d %s" % contname
        print(exec_string_forw1)
        self.__class__.node_ids.append(subprocess.check_output(exec_string_forw1, shell=True).strip('\n'))

        exec_string_forw2 = sudo + " docker run -p 8085:8080 --net=mynet -e MAINIP=10.0.0.20:8080 -d %s" % contname
        print(exec_string_forw2)
        self.__class__.node_ids.append(subprocess.check_output(exec_string_forw2, shell=True).strip('\n'))

        self.__class__.nodes_address = ['http://' + hostname + ":" + x for x in self.__class__.host_ports]
        # print(self.__class__.node_ids) #un-comment this line to display container IDs

    def setUp(self):

        if not self.__class__.has_been_run_once:
            self.__class__.has_been_run_once = True
            self.spin_up_nodes()
            print("Sleeping for 10 seconds to let servers bootup")
            time.sleep(10)

# put key on primary, get key on proxy1
    def test_a_primary_to_proxy (self):
        res = requests.put(self.__class__.nodes_address[0] + '/keyValue-store/primary', data = {'val':self.__class__.val2})
        res = requests.get(self.__class__.nodes_address[1] + '/keyValue-store/primary')
        d = res.json()
        self.assertEqual(res.status_code, 200)

# put key on proxy1, get key on proxy2
    def test_b_proxy_to_proxy (self):
        res = requests.put(self.__class__.nodes_address[1] + '/keyValue-store/proxy', data = {'val':self.__class__.val2})
        res = requests.get(self.__class__.nodes_address[2] + '/keyValue-store/proxy')
        d = res.json()
        self.assertEqual(res.status_code, 200)

# delete key on proxy1, get key on primary
    def test_c_proxy_to_primary (self):
        res = requests.delete(self.__class__.nodes_address[1] + '/keyValue-store/proxy')
        res = requests.get(self.__class__.nodes_address[0] + '/keyValue-store/proxy')
        d = res.json()
        self.assertEqual(res.status_code, 404)

# get key (from primary) when proxy is down
    def test_d_proxy_down (self):
        res = requests.put(self.__class__.nodes_address[2] + '/keyValue-store/' + self.__class__.key1, data = {'val':self.__class__.val1})
        shell_command = "docker stop " + str(self.__class__.node_ids[2])
        subprocess.check_output(shell_command, shell=True)
        res = requests.get(self.__class__.nodes_address[0] + '/keyValue-store/' + self.__class__.key1)
        d = res.json()
        self.assertEqual(res.status_code, 200)

# attempt to get key (from proxy1) when primary is down
    def test_e_primary_down (self):
        res = requests.put(self.__class__.nodes_address[1] + '/keyValue-store/' + self.__class__.key1, data = {'val':self.__class__.val1})
        shell_command = "docker stop " + str(self.__class__.node_ids[0])
        subprocess.check_output(shell_command, shell=True)
        res = requests.get(self.__class__.nodes_address[1] + '/keyValue-store/' + self.__class__.key1)
        d = res.json()
        self.assertEqual(res.status_code, 501)

# dummy test to indicate end of tests
    def test_z_dummy(self):
        print("| Finished.")
        self.__class__.all_tests_done = True

    def tearDown(self):
        if self.__class__.all_tests_done:
            print("\nKilling all alive nodes.")
            shell_command = "docker stop $(docker ps -q)"
            subprocess.check_output(shell_command, shell=True)


# Use command-line arguments to set Docker options

parser = argparse.ArgumentParser()
parser.add_argument('--hostname', action='store', default='localhost',
                    help='Windows and Mac users set this to the docker vm ip')
parser.add_argument('-c', '--contname', action='store', required=True,
                    help='Set your container name here')
parser.add_argument('--sudo', action='store_true',  
                    help='Use if you need sudo to start containers')
args, rem = parser.parse_known_args()

sys.argv = sys.argv[:1] + rem #remove parsed args from command-line args list

hostname = args.hostname
contname = args.contname
sudo = 'sudo' if args.sudo else ''


if __name__ == '__main__':
    unittest.main()
